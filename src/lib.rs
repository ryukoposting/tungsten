#![no_std]
#![feature(asm)]
#![feature(lang_items)]
#![feature(start)]
#![feature(core_intrinsics)]
#![allow(non_snake_case)]
#![allow(non_shorthand_field_patterns)]
#![deny(unused_unsafe)]

pub mod foo;

